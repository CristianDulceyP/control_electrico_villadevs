from django.shortcuts import render
from puntos.models import Punto


def Home(request):
    puntos = Punto.objects.all()

    return render(request, 'home.html', {"puntos": puntos,})