import json
from channels import Group
from channels.sessions import channel_session
from django.contrib.humanize.templatetags import humanize
from django.utils import timezone

from puntos.models import Punto

def as_dict(a,b,d):
    return {
        'titulo': a,
        'rta':b,
        'fecha':d,
    }


@channel_session
def ws_connect2(message,id):
    room = message.content['path'].strip("/")
    # Save room in session and add us to the group
    message.channel_session['room'] = room
    Group("puntos-%s"%id).add(message.reply_channel)

# Connected to websocket.receive
@channel_session
def ws_receive2(message,id):
    # print (message.content['text'],)
    try:
        pun = Punto.objects.get(id=id)
    except:
        print ("no exixten puntos")
    if "_" in message.content['text']:
        pun = Punto.objects.get(id=id)
        aux = pun.valor
        if aux == True:
            pun.valor = False
        elif aux == False:
            pun.valor = True
        pun.fecha_ultima_modificacion = timezone.now()
        pun.save()
    rta = None
    titulo = None
    ultima = None
    fecha = None

    # print usu.valor
    if pun.valor:
        rta = 'btn red'
        titulo = "Apagado"

    else:
        rta = 'btn '
        titulo = "Encendido"



    fecha = u"%s" %(humanize.naturaltime(pun.fecha_ultima_modificacion))

    Group("puntos-%s"%id).send({
        "text": json.dumps(as_dict(titulo,rta,fecha)),
        # "text": u"%s" %rta,
    })


# Connected to websocket.disconnect
@channel_session
def ws_disconnect2(message,id):
    Group("verificar-%s"%id).discard(message.reply_channel)

#### verificaar ruta