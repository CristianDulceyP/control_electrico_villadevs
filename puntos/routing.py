# consumer que trae channels para archivos estaticos
from channels.staticfiles import StaticFilesConsumer

from puntos.consumers import ws_connect2, ws_receive2, ws_disconnect2
from . import consumers
from channels.routing import route, include

puntos_verificar = [
    # archivos staticos
    #route('http.request',StaticFilesConsumer()),
    route("websocket.connect", ws_connect2,path = r"^/(?P<id>[^/]+)"),
    route("websocket.receive", ws_receive2,path = r"^/(?P<id>[^/]+)"),
    route("websocket.disconnect", ws_disconnect2,path = r"^/(?P<id>[^/]+)"),
]

channel_routing = [
    include(puntos_verificar, path=r"^/puntos"),
]