from __future__ import unicode_literals
from django.db import models
try:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
except:
    pass


# Create your models here.
class Punto(models.Model):
    nombre = models.CharField(max_length=500)
    descripcion = models.TextField(blank=True, null=True)
    fecha_ultima_modificacion = models.DateTimeField(blank=True,null=True)
    valor = models.BooleanField(default=False)
    entrada = models.PositiveIntegerField(unique=True)
    class Meta:
        verbose_name = 'Punto'
        verbose_name_plural = 'Puntos'
    def __unicode__(self):
        return unicode(self.nombre)

    def save(self, *args, **kwargs):
        if self.id:
            try:
                GPIO.output(self.entrada, self.valor)
            except:
                pass
        super(Punto, self).save(*args, **kwargs)

try:
    puntos = Punto.objects.all()
    for a in puntos:
        GPIO.setup(a.entrada, GPIO.OUT)
        if a.id==1:
            GPIO.output(a.entrada, False)
            a.valor = False
        else:
            GPIO.output(a.entrada, True)
            a.valor = True
        a.save()
except:
    try:
        puntos = Punto.objects.all()
        for a in puntos:
            if a.id == 1:
                a.valor = False
            else:
                a.valor = True
            a.save()
    except:
        print("Creando Base De Datos")